package faust.providence.app.sqlitedemo

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import faust.providence.app.sqlitedemo.R.id.all
import kotlinx.android.synthetic.main.activity_simple_view.*

class SimpleViewActivity : AppCompatActivity() {

    private val participants: Participants by lazy { Participants(applicationContext)}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_view)

        var items: ArrayList<Participants.Item>

        val groupId = (if(intent.hasExtra("group")) this.intent.extras.getInt("group", -1) else -1)

        items = ( if(groupId<0) participants.all else participants.group(groupId))

        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, items)
        this.listView.adapter = adapter

        this.listView.setOnItemClickListener() { adapterView: AdapterView<*>, view1: View, i: Int, l: Long ->
            val intent = Intent(this,EditActivity::class.java)
            intent.putExtra("id",items[i].id)
            startActivity(intent)
        }
    }
}
